Batsim-related debian packages
==============================

.. warning::

   This is a dump of my attempt to package Batsim-related software with debian tools.
   I stopped it for sanity's sake and do not plan to continue this nonsense, but I'd be glad to help anyone motivated to package our tools — e.g., by sharing information about how to build our software: Please `contact us`_ to get this information (and your beers) in this case ;).

How to use the packages?
------------------------

1. Lie down.
2. Try not to cry.
3. Cry a lot.
4. Use a supported package manager (cf. `Batsim documentation`_).

How to build the packages?
--------------------------

Install the needed dependencies manually.

.. code-block:: bash

   # note: privileges required
   export DEBIAN_FRONTEND=noninteractive
   apt update
   apt install -yq debmake

Generate the package. For example, for intervalset_:

.. code-block:: bash

   # move into the package
   cd intervalset-1.2.0

   # install the required dependencies (didn't find how to automate this)
   apt install -yq meson ninja-build libboost-dev

   # generate the package
   debuild -us -uc

Resources
---------

- Package example (SimGrid): https://salsa.debian.org/debian/simgrid
- Getting started: https://www.debian.org/doc/manuals/debmake-doc/ch04.en.html
- Requested files in debian dir: https://www.debian.org/doc/manuals/maint-guide/dreq.html
- Other files in debian dir: https://www.debian.org/doc/manuals/maint-guide/dother.html
- More doc: https://www.debian.org/doc/debian-policy/ch-controlfields.html

.. _intervalset: https://framagit.org/batsim/intervalset
.. _Batsim documentation: https://batsim.readthedocs.io/en/latest/installation.html
.. _contact us: https://batsim.readthedocs.io/en/latest/contact.html
