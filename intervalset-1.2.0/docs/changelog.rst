Changelog
=========

All notable changes to this project will be documented in this file.
The format is based on `Keep a Changelog`_ and
intervalset adheres to `Semantic Versioning`_.
The public API of intervalset is simply the public C++ functions and types
defined by the library.

........................................................................................................................

Unreleased
----------

- `Commits since v1.2.0 <https://framagit.org/batsim/intervalset/compare/v1.2.0...master>`_

........................................................................................................................

v1.2.0
------

- `Commits since v1.1.0 <https://framagit.org/batsim/intervalset/compare/v1.1.0...v1.2.0>`_
- Release date: 2019-02-22

Added
~~~~~

- New ``is_empty`` method, that returns whether an intervalset is empty.
- Full API is now documented on readthedocs.

........................................................................................................................

v1.1.0
------

- `Commits since v1.0.0 <https://framagit.org/batsim/intervalset/compare/v1.0.0...v1.1.0>`_
- Release date: 2018-11-09

Changed
~~~~~~~

-  Build system changed from CMake to Meson.

........................................................................................................................

v1.0.0
------

- Release date: 2018-10-16

.. _Keep a Changelog: http://keepachangelog.com/en/1.0.0/
.. _Semantic Versioning: http://semver.org/spec/v2.0.0.html
