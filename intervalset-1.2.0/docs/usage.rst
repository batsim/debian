Usage
=====

How to link?
------------
**intervalset** must be linked if one wants to use it.

- Either with ``-lintervalset``
- Or ``-lintervalset_static`` (to bundle **intervalset** with your program)

Quick example
-------------
.. literalinclude:: ../test/usage_example.cpp
    :language: c++
