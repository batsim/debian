intervalset documentation
=========================

**intervalset** is a C++ library to manage sets of closed intervals of integers.
This is a simple wrapper around `Boost.Icl`_.

.. toctree::
   :maxdepth: 2

   install
   usage
   api
   reference
   changelog

.. _`Boost.Icl`: https://www.boost.org/doc/libs/release/libs/icl/doc/html/index.html
