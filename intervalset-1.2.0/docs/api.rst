API showcase
============

Constructors
------------
.. literalinclude:: ../test/constructors_example.cpp
    :language: c++

Set operations
--------------
.. literalinclude:: ../test/set_operations_example.cpp
    :language: c++

Accessing elements
------------------
.. literalinclude:: ../test/access_example.cpp
    :language: c++

Testing membership
------------------
.. literalinclude:: ../test/membership_example.cpp
    :language: c++

Iterating elements and intervals
--------------------------------
.. literalinclude:: ../test/traversal_example.cpp
    :language: c++
